import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = {
  docTypes: [],
};

const getters = {
  getDocTypes(state) {
    return state.docTypes;
  },
};

const mutations = {
  setDocTypes(state, payload) {
    state.docTypes = payload;
  }
};

const actions = {
  fetchDocTypes({ commit }) {
    // The first way for axios (Recommended by axios):
    // We can keep the data in state and update.
    axios.get("http://localhost:8091/restApiApplication/docType/getDocTypes")
      .then((response) => {
        commit('setDocTypes', response.data);
      })
      .catch(() => {
        // if backend is not working. this data assigned to docyTypes
        const mockData = [
            {
                "id": 1,
                "name": "Sipariş"
            },
            {
                "id": 2,
                "name": "Fatura"
            },
            {
                "id": 3,
                "name": "İrsaliye"
            },
            {
                "id": 4,
                "name": "İrsaliyeli Fatura"
            },
            {
                "id": 5,
                "name": "Iptal"
            }
        ];

        commit('setDocTypes', mockData);
      });
  },
  fetchMaterials() {
    // Second and my preferred route throughout the project is;
    // keeping and updating data in a component
    return axios.get("http://localhost:8091/restApiApplication/material/getMaterial");
  },
  saveMaterial(_, parameters) {
    return axios.post("http://localhost:8091/restApiApplication/material/saveMaterial", parameters);
  },
  fetchCustomers() {
    return axios.get("http://localhost:8091/restApiApplication/customer/getCustomer");
  },
  saveCustomer(_, parameters) {
    return axios.post("http://localhost:8091/restApiApplication/customer/saveCustomer", parameters);
  },
  fetchSalesHeads() {
    return axios.get("http://localhost:8091/restApiApplication/salesHead/getSalesHead");
  },
  saveSalesHead(_, parameters) {
    return axios.post("http://localhost:8091/restApiApplication/salesHead/saveSalesHead", parameters);
  },
  fetchSalesHeadById(_, salesHeadId) {
    return axios.get(`http://localhost:8091/restApiApplication/salesHead/getSalesHead/${salesHeadId}`);
  },
};

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
});

export default store;
